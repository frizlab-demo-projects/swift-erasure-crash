/*
 * Protocol.swift
 * test_crash
 *
 * Created by François Lamboley on 10/07/2019.
 */

import Foundation



public protocol ParentProtocol {
	
	associatedtype TypeInParent
	
}


public protocol ChildProtocol : ParentProtocol {
}
