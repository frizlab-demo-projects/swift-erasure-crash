/*
 * ErasureParent.swift
 * test_crash
 *
 * Created by François Lamboley on 10/07/2019.
 */

import Foundation



private protocol ParentProtocolBox {
}

private struct ConcreteParentProtocolBox<Base : ParentProtocol> : ParentProtocolBox {
	
	let originalParent: Base
	
}

public class AnyParent : ParentProtocol {
	
	public typealias TypeInParent = Any
	
	init<T : ParentProtocol>(_ object: T) {
		box = ConcreteParentProtocolBox(originalParent: object)
	}
	
	public func unboxed<ParentType : ParentProtocol>() -> ParentType? {
		return (box as? ConcreteParentProtocolBox<ParentType>)?.originalParent ?? (box as? ConcreteParentProtocolBox<AnyParent>)?.originalParent.unboxed()
	}
	
	private let box: ParentProtocolBox
	
}
