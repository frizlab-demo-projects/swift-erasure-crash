/*
 * ErasureChild.swift
 * test_crash
 *
 * Created by François Lamboley on 10/07/2019.
 */

import Foundation



private protocol ChildProtocolBox {
}

private struct ConcreteChildProtocolBox<Base : ChildProtocol> : ChildProtocolBox {
	
	let originalChild: Base
	
}

public class AnyChild : AnyParent, ChildProtocol {
	
	override init<T : ChildProtocol>(_ object: T) {
		box = ConcreteChildProtocolBox(originalChild: object)
		super.init(object)
	}
	
	public override func unboxed<ChildType : ChildProtocol>() -> ChildType? {
		return (box as? ConcreteChildProtocolBox<ChildType>)?.originalChild ?? (box as? ConcreteChildProtocolBox<AnyChild>)?.originalChild.unboxed()
	}
	
	private let box: ChildProtocolBox
	
}
