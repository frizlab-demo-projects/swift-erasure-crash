/*
 * Implementation.swift
 * test_crash
 *
 * Created by François Lamboley on 10/07/2019.
 */

import Foundation



public class ParentImplementation : ParentProtocol {
	
	public typealias TypeInParent = Void
	
}

public class ChildImplementation : ParentImplementation, ChildProtocol {
}
