import XCTest

import test_crashTests

var tests = [XCTestCaseEntry]()
tests += test_crashTests.allTests()
XCTMain(tests)
