# test_crash

This project crashes when compiled in release mode.
Tested with Swift 4.2.1 on Linux, Swift 5.0.1 and 5.1 (swiftlang-1100.0.212.5 clang-1100.0.28.2) on macOS.
